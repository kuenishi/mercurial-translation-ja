#!/usr/bin/env python

import sys
import optparse
import polib

optparser = optparse.OptionParser("""%prog [options] pofile [target ...]

This picks untranslated/fuzzy messages up from specified '*.po' file.

This only picks up messages occured in 'target' source files, if specified.
'target' has to match exactly against filename listed in 'occurrence'
comment (started with '#:') of '*.po' file.

Both fuzzy and untranslated messages are picked up, if neither '--fuzzy'
nor '--untranslated' is specified.
""")
optparser.add_option("", "--fuzzy",
                     help="pick fuzzy messages up ",
                     action="store_true")
optparser.add_option("", "--untranslated",
                     help="pick untranslated messages up",
                     action="store_true")
(options, args) = optparser.parse_args()

if not args:
    sys.stderr.write("%s:error: at least one *.po file is required\n"
                     % (sys.argv[0]))
    sys.exit(1)

# replace polib._POFileParser to show linenum of problematic msgstr
class ExtPOFileParser(polib._POFileParser):
    def process(self, symbol, linenum):
        super(ExtPOFileParser, self).process(symbol, linenum)
        if symbol == 'MS': # msgstr
            self.current_entry.linenum = linenum
polib._POFileParser = ExtPOFileParser

pofilename = args[0]
pofile = polib.pofile(pofilename)
if 1 < len(args):
    targets = set(args[1:])
    def match(pe):
        for f, l in pe.occurrences:
            if f in targets:
                return True
else:
    match = lambda pe: True

if not options.untranslated and not options.fuzzy:
    options.untranslated = options.fuzzy = True

poentries = []
if options.untranslated:
    for pe in pofile.untranslated_entries():
        if match(pe):
            poentries.append(pe)
if options.fuzzy:
    for pe in pofile.fuzzy_entries():
        if match(pe):
            poentries.append(pe)

for pe in sorted(poentries, lambda x, y: x.linenum - y.linenum):
    sys.stderr.write("%s:%d: %s\n"
                     % (pofilename, pe.linenum,
                        'fuzzy' in pe.flags
                        and 'marked as fuzzy' or 'not yet translated'))

if poentries:
    sys.exit(1)
