#!/usr/bin/env python
#
# check-translation.py - check Mercurial specific translation problems

import polib
import re
import os
import unicodedata

checkers = []

def checker(level, msgidpat):
    def decorator(func):
        if msgidpat:
            match = re.compile(msgidpat).search
        else:
            match = lambda msgid: True
        checkers.append((func, level))
        func.match = match
        return func
    return decorator

def match(checker, pe):
    """Examine whether POEntry "pe" is target of specified checker or not
    """
    if not checker.match(pe.msgid):
        return
    # examine suppression by translator comment
    nochecker = 'no-%s-check' % checker.__name__
    for tc in pe.tcomment.split():
        if nochecker == tc:
            return
    return True

####################

def fatalchecker(msgidpat=None):
    return checker('fatal', msgidpat)

@fatalchecker(r'\$\$')
def promptchoice(pe):
    """Check translation of the string given to "ui.promptchoice()"

    >>> pe = polib.POEntry(
    ...     msgid ='prompt$$missing &sep$$missing &amp$$followed by &none',
    ...     msgstr='prompt  missing &sep$$missing  amp$$followed by none&')
    >>> match(promptchoice, pe)
    True
    >>> for e in promptchoice(pe): print e
    number of choices differs between msgid and msgstr
    msgstr has invalid choice missing '&'
    msgstr has invalid '&' followed by none
    """
    idchoices = [c.rstrip(' ') for c in pe.msgid.split('$$')[1:]]
    strchoices = [c.rstrip(' ') for c in pe.msgstr.split('$$')[1:]]

    if len(idchoices) != len(strchoices):
        yield "number of choices differs between msgid and msgstr"

    indices = [(c, c.find('&')) for c in strchoices]
    if [c for c, i in indices if i == -1]:
        yield "msgstr has invalid choice missing '&'"
    if [c for c, i in indices if len(c) == i + 1]:
        yield "msgstr has invalid '&' followed by none"

####################

def warningchecker(msgidpat=None):
    return checker('warning', msgidpat)

@warningchecker()
def taildoublecolons(pe):
    """Check equality of tail '::'-ness between msgid and msgstr

    >>> pe = polib.POEntry(
    ...     msgid ='ends with ::',
    ...     msgstr='ends with ::')
    >>> for e in taildoublecolons(pe): print e
    >>> pe = polib.POEntry(
    ...     msgid ='ends with ::',
    ...     msgstr='ends without double-colons')
    >>> for e in taildoublecolons(pe): print e
    tail '::'-ness differs between msgid and msgstr
    >>> pe = polib.POEntry(
    ...     msgid ='ends without double-colons',
    ...     msgstr='ends with ::')
    >>> for e in taildoublecolons(pe): print e
    tail '::'-ness differs between msgid and msgstr
    """
    if pe.msgid.endswith('::') != pe.msgstr.endswith('::'):
        yield "tail '::'-ness differs between msgid and msgstr"

@warningchecker()
def indentation(pe):
    """Check equality of initial indentation between msgid and msgstr

    This may report unexpected warning, because this doesn't aware
    the syntax of rst document and the context of msgstr.

    >>> pe = polib.POEntry(
    ...     msgid ='    indented text',
    ...     msgstr='  narrowed indentation')
    >>> for e in indentation(pe): print e
    initial indentation width differs betweeen msgid and msgstr
    """
    idindent = len(pe.msgid) - len(pe.msgid.lstrip())
    strindent = len(pe.msgstr) - len(pe.msgstr.lstrip())
    if idindent != strindent:
        yield "initial indentation width differs betweeen msgid and msgstr"

sectionmarksre = re.compile(r'^(?:"{2,}|={2,}|-{2,}|\.{2,}|#{2,})$')

# How to treat ambiguous-width characters. Set to 'wide' to treat as wide.
wide = (os.environ.get("HGENCODINGAMBIGUOUS", "narrow") == "wide"
        and "WFA" or "WF")

def ucolwidth(d):
    eaw = getattr(unicodedata, 'east_asian_width', None)
    if eaw is not None:
        return sum([eaw(c) in wide and 2 or 1 for c in d])
    return len(d)

@warningchecker(r'(?m)^(?:"{2,}|={2,}|-{2,}|\.{2,}|#{2,})$')
def sectionmarks(pe):
    """Check section marks and title text

    >>> pe = polib.POEntry(
    ...     msgid =(u'========================\\n'
    ...              'overline-underline style\\n'
    ...              '========================'),
    ...     msgstr=(u'==========\\n'
    ...              'multiple title\\n'
    ...              'text lines\\n'
    ...              '\\n'
    ...              '----------------\\n'
    ...              '----------------\\n'
    ...              '\\n'
    ...              '=============\\n'
    ...              'mark mismatch\\n'
    ...              '.............\\n'
    ...              '################\\n'
    ...              'length mismatch\\n'
    ...              '#############\\n'
    ...              '\\n'
    ...              '============\\n'
    ...              'no underline'))
    >>> match(sectionmarks, pe)
    True
    >>> for e in sectionmarks(pe): print e
    multiple title text lines for section mark '='
    no title text for section mark '-'
    section mark differs between overline ('=') and underline ('.')
    length of section mark '#' differs between overline and underline
    no underline corresponed to overline for section mark '='
    >>> pe = polib.POEntry(
    ...     msgid =(u'underline style\\n'
    ...              '==============='),
    ...     msgstr=(u'length mismatch\\n'
    ...              '============\\n'
    ...              'long normal text\\n'
    ...              'title text\\n'
    ...              '----------'))
    >>> match(sectionmarks, pe)
    True
    >>> for e in sectionmarks(pe): print e
    column width differs between title text and section mark '='
    """
    titletext = overline = None
    for l in pe.msgstr.split('\n'):
        if overline: # overline-underline style
            if not sectionmarksre.match(l):
                if titletext:
                    yield ("multiple title text lines for section mark %r"
                           % (overline[0].encode('ascii')))
                    titletext = overline = None # clear current context
                else:
                    titletext = l
                continue

            if not titletext:
                yield ("no title text for section mark %r"
                       % (overline[0].encode('ascii')))
            if overline[0] != l[0]:
                yield ("section mark differs"
                       " between overline (%r) and underline (%r)"
                       % (overline[0].encode('ascii'), l[0].encode('ascii')))
            if len(overline) != len(l):
                yield ("length of section mark %r differs"
                       " between overline and underline"
                       % (overline[0].encode('ascii')))
        else:
            if not l.strip():
                continue # skip empty line
            if not sectionmarksre.match(l):
                titletext = l
                continue
            if not titletext: # overline for overline-underline style
                overline = l
                continue

            # this length check is needed, because:
            # - section title in underline style is processed also by
            #   Mercurial builtin minirst, and
            # - minirst recognizes section titlte correctly, only when
            #   column width is same between title text and section marks
            if ucolwidth(titletext) != len(l):
                yield ("column width differs"
                       " between title text and section mark %r"
                       % (l[0].encode('ascii')))

        titletext = overline = None # clear current context

    if overline:
        yield ("no underline corresponed to overline for section mark %r"
               % (overline[0].encode('ascii')))

####################

def check(pofile, fatal=True, warning=False):
    targetlevel = { 'fatal': fatal, 'warning': warning }
    targetcheckers = [(checker, level)
                      for checker, level in checkers
                      if targetlevel[level]]
    if not targetcheckers:
        return []

    detected = []
    for pe in pofile.translated_entries():
        errors = []
        for checker, level in targetcheckers:
            if match(checker, pe):
                errors.extend((level, checker.__name__, error)
                              for error in checker(pe))
        if errors:
            detected.append((pe, errors))
    return detected

########################################

if __name__ == "__main__":
    import sys
    import optparse

    optparser = optparse.OptionParser("""%prog [options] pofile ...

This checks Mercurial specific translation problems in specified
'*.po' files.

Each detected problems are shown in the format below::

    filename:linenum:type(checker): problem detail .....

"type" is "fatal" or "warning". "checker" is the name of the function
detecting corresponded error.

Checking by checker "foo" on the specific msgstr can be suppressed by
the "translator comment" like below. Multiple "no-xxxx-check" should
be separated by whitespaces::

    # no-foo-check
    msgid = "....."
    msgstr = "....."
""")
    optparser.add_option("", "--warning",
                         help="show also warning level problems",
                         action="store_true")
    optparser.add_option("", "--doctest",
                         help="run doctest of this tool, instead of check",
                         action="store_true")
    (options, args) = optparser.parse_args()

    if options.doctest:
        import doctest
        failures, tests = doctest.testmod()
        sys.exit(failures and 1 or 0)

    # replace polib._POFileParser to show linenum of problematic msgstr
    class ExtPOFileParser(polib._POFileParser):
        def process(self, symbol, linenum):
            super(ExtPOFileParser, self).process(symbol, linenum)
            if symbol == 'MS': # msgstr
                self.current_entry.linenum = linenum
    polib._POFileParser = ExtPOFileParser

    detected = []
    warning = options.warning
    for f in args:
        detected.extend((f, pe, errors)
                        for pe, errors in check(polib.pofile(f),
                                                warning=warning))
    if detected:
        for f, pe, errors in detected:
            for level, checker, error in errors:
                sys.stderr.write('%s:%d:%s(%s): %s\n'
                                 % (f, pe.linenum, level, checker, error))
        sys.exit(1)
