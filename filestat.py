#!/usr/bin/env python

files = {} # filename => (# of msgid, # of translated msgstr, # of fuzzy)

lineno = 0

infostock = ['', '', False, {}] # msgid, msgstr, fuzzy, filename map

def undefined_state(value):
    raise BaseException('unexpected invocation:%d: %s' % (lineno, value))

def wait_msgid(value):
    if 0 != len(value):
        global state
        infostock[0] = value
        state = skip_value

def wait_msgstr(value):
    if 0 != len(value):
        global state
        infostock[1] = value
        state = skip_value

def skip_value(value):
    pass

state = undefined_state

####################

def comment_strategy(line):
    if line.startswith(':'):
        line = line[1:]
        for entry in line.split():
            entry = entry.strip()
            if 0 == len(entry): continue
            filename, lineno = entry.split(':')
            filenamemap = infostock[3]
            filenamemap[filename]  = filename
    else:
        for entry in line.split(','):
            if entry.strip() == 'fuzzy':
                infostock[2] = True

def msgid_strategy(line):
    id = line[1:-1] # eliminate quotations
    if 0 != len(id):
        global state
        infostock[0] = id
        state = skip_value
    else:
        state = wait_msgid

def msgstr_strategy(line):
    str = line[1:-1] # eliminate quotations
    if 0 != len(str):
        global state
        infostock[1] = str
        state = skip_value
    else:
        state = wait_msgstr

def value_strategy(line):
    str = line[1:-1] # eliminate quotations
    state(str)

####################

top_strategies = [
    ('#', comment_strategy),
    ('msgid ', msgid_strategy),
    ('msgstr ', msgstr_strategy),
    ('"', value_strategy),
]

####################

import sys
import optparse

optionlist = [
    optparse.make_option("-O", '--omit-finished',
                         help="omit finished files",
                         action="store_const", const=1, dest="omit_finished"
                         ),
    optparse.make_option("-l", '--list-filename',
                         help="show only filenames",
                         action="store_const", const=1, dest="list_filenames"
                         ),
]

optparser = optparse.OptionParser(usage='USAGE: %prog [options]',
                                  option_list=optionlist)

(options, args) = optparser.parse_args()

if 0 != len(args):
    optparser.error('too many arguments')

####################

for line in sys.stdin:
    line = line.strip()
    lineno += 1

    for prefix, strategy in top_strategies:
        if line.startswith(prefix):
            line = line[len(prefix):] # trim off
            strategy(line)
            break
    else:
        if 0 != len(line):
            raise BaseException('unexpected line: %s' % line)

        if 0 == len(infostock[0]):
            continue

        # empty line flush current status
        for filename in infostock[3].keys():
            filestat = files.setdefault(filename, [0, 0, 0])
            filestat[0] = filestat[0] + 1
            if 0 != len(infostock[1]): # already translated
                filestat[1] = filestat[1] + 1
            if infostock[2]: # fuzzy
                filestat[2] = filestat[2] + 1

        # clear information
        infostock = ['', '', False, {}]
        state = undefined_state

format = '%-40s % 6s % 6s (% 6s) %6s'

if not options.list_filenames:
    print format % (' filename:', 'msgid', 'done', 'rest', 'fuzzy')
for filename, stat in sorted(files.items()):
    if options.omit_finished and (0 == (stat[0] - stat[1])) and (0 == stat[2]):
        continue
    if options.list_filenames:
        print ('%s' % (filename))
    else:
        print (format %
               (filename, str(stat[0]), str(stat[1]),
                str(stat[0] - stat[1]), str(stat[2])))
