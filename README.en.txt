-------------------------------------------------------------------------------
                  Japanese translation for Mercurial
-------------------------------------------------------------------------------

This is the project to translate message file('i18n/ja.po') of
Mercurial distributed SCM(http://www.selenic.com/mercurial/wiki/).


* Terminology:

  - SRC      : source repo for Mercurial 
  - L10N-repo: translation working repo


* How to coordinate translation work in branches:

  - for translator:

    - [TRANSLATE] in 'default' branch of L10N-repo

      1. notify favorite translation part to maintainers and wait for
         acceptance of it

      2. translate corresponding part of 'ja.po'

         keywords included in '#' line preceding msgid means:

            - python-format:
              msgid is using "%xx"
              => useful to validate %xx usage by "msgfmt --check"

            - fuzzy:
              msgmerge detected modification of msgid
              => re-translation may be needed

         So, editing '#' line preceding msgid in ja.po file should be
         limited to eliminating fuzzy keyword at re-translation for
         msgid change

         Embed "XXXX" in translation to indicate that translation is anxious.

         If you need "filename + lineno" information to locate own
         assigned part, embed them into 'ja.po' by command invocation
         shown below

           % msgmerge --update L10N-repo/ja.po L10N-repo/hg.pot

         * use '-o destfile' instead of '--update', if you want to
           write out to file other than 'ja.po'

         You should eliminate these embedded information before
         committing in L10N-repo by command invocation shown below.

           % msgcat --no-location -o L10N-repo/ja.po L10N-repo/ja.po

         By below invocation, you can pick up important points in your
         assigned part from 'ja.po' in which these information are
         embedded.

           % python L10N-repo/pickup.py L10N-repo/ja.po assigned-filename ...

         Above command can detect those points:

           - 'not yet translated'
           - 'fuzzy msgid'
           - msgstr has multiple lines not including '\n' at the end of line
             ('unexpected wrap')

         Output format is "ja.po filename:line#:detail", as same as
         grep output format.

         * refer source file at the revision described in
           BASE_REVISION file, if you work with it

      3. check translation file format, and resolve problems, if you
         find out:

           % msgfmt --check L10N-repo/ja.po

         This invocation would show invalid usage of substitution
         format(such as '%x'), or invalid amount of
         new-line-char('\n')
         
         * if you embed location information into file other than
           'ja.po', check it before overwriting 'ja.po'

      4. check translated messages in steps shown below, and resolve
         problem if you find out

           % cd SRC
           % hg update `cat L10N-repo/BASE_REVISION`
           % cp L10N-repo/ja.po ./i18n/ja.po
           % make install
           % export LANGUAGE=ja
           % # re-produce situation when translated messages are shown

         * do not forget to configure PYTHONPATH env, if you install
           HG by "make install-home-bin" and so on

         You can check translation result more easily, if you have
         already installed target version of Mercurial.

           % cd L10N-repo
           % msgfmt -v -c -o hg.mo ja.po
           % cp hg.mo SITE_PACKAGES/mercurial/locale/ja/LC_MESSAGES

         * SITE_PACKAGES is the location where Mercurial Python
           modules are installed, "/usr/lib/python2.5/site-packages"
           for example.

      5. create patch to update 'ja.po' and send it to
         maintainers, if not having public repository

         * do not forget to eliminate "filename + lineno" information,
           if you embedded them

      6. commit update of 'ja.po' and notify it to maintainers,
         if having public repository

         * do not forget to eliminate "filename + lineno" information,
           if you embedded them

      7. go back to step 3 for translation adjustment, if needed
         (e.g.: update of source documentation, style unification, and so on)

      8. (completed)

  - for maintainer:

    - apply MERGE process on demand
    - apply CATCHUP process at short intervals(weekly ?)
    - apply IMPORT process at 'i18n/ja.po' update in SRC
    - apply EXPORT process at long intervals(bimonthly ?)
    - check '#, fuzzy' parts in 'i18n/ja.po' found in UPDATE process
      and coordinate re-translation of them


    * [MERGE] changeset from translators:

      0. check below points

           - msgfmt --check
           - "filename + lineno" information are NOT included in 'ja.po'

      1. apply received patch on head in 'default' branch of
         L10N-repo, if translation is received by patch form

      2. 'hg pull' from repository of translator(and resolve multiple
         heads), if translation is published via public repository

      3. check translation, and request translator to re-translate, if
         needed

      4. Update translation dictionary(placed in head of ja.po file)
         or "Style of translation"(placed in tail of 'README.ja.txt'),
         if needed

      5. (completed)


    * [UPDATE] 'ja.po' of L10N-repo by 'hg.pot' of SRC:

      1. check 'ja.po' content of L10N-repo by:

           % msgfmt --check L10N-repo/ja.po

      2. resolve problems found and commit result, if needed

      3. update SRC by downloading tarball or pull-n-update

      4. update 'hg.pot' in SRC by:

           % make update-pot

      5. over-write 'hg.pot' of L10N-repo by new one in SRC

      6. update 'ja.po' of L10N-repo with updated 'hg.pot' in L10N-repo by:

           % msgmerge --update --no-location \
                 L10N-repo/ja.po L10N-repo/hg.pot

      7. Write revision of SRC into L10N-repp/BASE_REVISION

      8. commit updated 'hg.pot', 'ja.po' and BASE_REVISION in L10N-repo

      9. resolve 'fuzzy' problems in updated 'ja.po', if needed

         DO ONLY adjustment relation between msgid/msgstr in this
         step.

         DO NOT adjust translation part in non-'default' branch, even
         if it is very simple adjustment.

         Keep '#, fuzzy' mark in 'ja.po' file to find target out
         easily for re-translation.

      10. commit updated 'ja.po' in L10N-repo

      11. check translation statistics, and request assigned
          translator to re-translate, if needed

            % msgmerge -o - L10N-repo/ja.po L10N-repo/hg.pot |
              python L10N-repo/filestat.py |
              grep -f L10N-repo/assigned.txt

          * output format of above is: 'filename msgid done fuzzy'

      11. (completed)


    * [CATCHUP] update in SRC:

      1. merge from 'default' into 'downstream-line' branch of L10N-repo

      2. apply UPDATE process on head in 'downstream-line' branch of
         L10N-repo

      3. merge UPDATE-ed revision into 'default' branch of L10N-repo

      4. (completed)


    * [IMPORT] from SRC into L10N-repo:

      1. update SRC

      2. over-write 'ja.po' in 'sync-line' branch head of L10N-repo by
         new one in SRC

      3. do below steps, only if there is any incoming update of 'ja.po'

      4. commit 'ja.po' update in 'sync-line' branch of L10N-repo

      5. merge above new revision into 'downstream-line' branch of
         L10N-repo

      6. merge from 'default' into 'downstream-line' branch of L10N-repo

      7. apply UPDATE process on merged revision in
         'downstream-line' branch of L10N-repo

      8. merge UPDATE-ed revision into 'default' branch of L10N-repo

      9. (completed)


    * [EXPORT] from L10N-repo:

      0. finish CATCHUP process, and resolve all problems
         (also update PO-Revision-Date information in 'ja.po')

      1. decide exported revision in 'default' branch of L10N-repo

      2. merge above revision into 'upstream-line' branch of L10N-repo

      3. get difference of 'ja.po' from head of 'sync-line' branch to
         head of 'upstream-line' branch

      4. send above difference as updating patch for 'i18n/ja.po' to
         mercurial-devel ML

      5. resolve minor problems in 'upstream-line' branch of L10N-repo
         and re-send patch, if needed(e.g.: typo and so on)

      6. merge above two heads into 'sync-line', when sent patch is
         imported into official repo

      7. (completed)


* Style of translation:

  Please see 'README.ja.txt' for detail, because I can not explain it
  well in English :-<

-------------------------------------------------------------------------------
